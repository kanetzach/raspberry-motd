[Screenshot](https://www.delcid.eu/pic/uploads/big/13b73442fb2a89434c59349f54b20c68.png)

This is a MOTD for Raspbian inspired by several MOTDs out there, merging some of their ideas and using some of my own.

Usage:

- Copy the file motd.sh into /etc/profile.d/
- Enable PrintMod in /etc/ssh/sshd_config
- Restart sshd service (systemctl restart ssh)
- Delete the content (not the folder) of /etc/update-motd.d and /etc/motd
- Edit motd.sh with the name of your own machine

