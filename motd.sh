#!/bin/bash
let upSeconds="$(/usr/bin/cut -d. -f1 /proc/uptime)"
let secs=$((${upSeconds}%60))
let mins=$((${upSeconds}/60%60))
let hours=$((${upSeconds}/3600%24))
let days=$((${upSeconds}/86400))
load=$(cat /proc/loadavg | awk '{print $1}')
memory_usage=$(free -m | awk '/Mem/ { printf("%3.1f%%", $3/$2*100) }')
root_usage=$(df -h / | awk '/\// {print $(NF-1)}')
swap_usage=$(free -m | awk '/Swap/ { printf("%3.1f%%", $3/$2*100) }')
users=$(users | wc -w)
UPTIME=$(printf "%d days, %02dh%02dm%02ds" "$days" "$hours" "$mins" "$secs")
TEMP=$(cat /sys/class/thermal/thermal_zone*/temp | cut -c 1-2)

[ -r /etc/lsb-release ] && . /etc/lsb-release
if [ -z "$DISTRIB_DESCRIPTION" ] && [ -x /usr/bin/lsb_release ]; then
        # Fall back to using the very slow lsb_release utility
        DISTRIB_DESCRIPTION=$(lsb_release -s -d)
fi
echo "$(tput setaf 4)
   ██╗  ██╗██╗██╗   ██╗ █████╗
   ██║ ██╔╝██║██║   ██║██╔══██╗
   █████╔╝ ██║██║   ██║███████║
   ██╔═██╗ ██║╚██╗ ██╔╝██╔══██║
   ██║  ██╗██║ ╚████╔╝ ██║  ██║
   ╚═╝  ╚═╝╚═╝  ╚═══╝  ╚═╝  ╚═╝$(tput setaf 1)
   > $(tput setaf 2)Date$(tput setaf 1)................:$(tput setaf 3) `date +"%A, %e %B %Y, %r"` $(tput setaf 1)
   > $(tput setaf 2)Version$(tput setaf 1).............:$(tput setaf 3) "$DISTRIB_DESCRIPTION" "$(uname -r)" $(tput setaf 1)
   > $(tput setaf 2)Uptime$(tput setaf 1)..............:$(tput setaf 3) ${UPTIME} $(tput setaf 1)

   > $(tput setaf 2)Load$(tput setaf 1)................:$(tput setaf 3) "$load" $(tput setaf 1)// $(tput setaf 2)Processes$(tput setaf 1)..........:$(tput setaf 3) `ps ax | wc -l | tr -d " "` $(tput setaf 1)
   > $(tput setaf 2)Memory Usage$(tput setaf 1)........:$(tput setaf 3) "$memory_usage" $(tput setaf 1)
   > $(tput setaf 2)Disk Usage$(tput setaf 1)..........:$(tput setaf 3) "$root_usage" $(tput setaf 1) // $(tput setaf 2)Swap usage$(tput setaf 1).........:$(tput setaf 3) "$swap_usage"$(tput setaf 1)
   > $(tput setaf 2)Online Users$(tput setaf 1)........:$(tput setaf 3) "$users" $(tput setaf 1)   // $(tput setaf 2)Temperature$(tput setaf 1)........:$(tput setaf 3) "$TEMP"c$(tput setaf 1)
   $(tput sgr0)"

